# Project 6: Brevet time calculator service

Student Modifier: Bryan Carl bcarl@uoregon.edu

Originally written by Ram Duraijana

Simple list of controle times stored in MongoDB database.

## What is in this repository

A small ACP Times calculator. Edited for functionality on a server-side basis. 

## Time Calculations

When a distance in kilometers is divided by a speed in kilometers per hour, the result is a time measured in hours. For example, a distance of 100 km divided by a speed of 15 km per hour results in a time of 6.666666... hours. To convert that figure into hours and minutes, subtract the whole number of hours (6) and multiply the resulting fractional part by 60. The result is 6 hours, 40 minutes, expressed here as 6H40.

The calculator converts all inputs expressed in units of miles to kilometers and roundws the result to the nearest kilometer before being used in calculations. Times are rounded to the nearest minute.

## Functionality

Almost everything in this repository is implemented based on the port 5001 for testing. If you want anything to display you will have to use the 5001 port.

The 5000 port is for inputting times into the database and using the 5001 port is how you will access the database.

localhost:5001/listAll

localhost:5001/listOpenOnly

localhost:5001/listCloseOnly

localhost:5001/listAll/csv

localhost:5001/listAll/json?top=5

etc, etc

This will be how the serverside URI's will be exposed. You will input on port 5000 and search on port 5001. I was able to complete all the necessary functionalities in time, so hopefully they work as intended. Thank you for your time, and please feel free to contct me if you have any questions!