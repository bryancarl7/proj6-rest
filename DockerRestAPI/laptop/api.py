# Laptop Service

import os
import arrow
import pymongo
import json
import io
import csv
from bson import ObjectId
from flask import Flask, redirect, url_for, request, render_template, jsonify
from flask_restful import Resource, Api, reqparse, fields
from pymongo import MongoClient
from acp_times import open_time, close_time

resource_fields = {
    'csv':   fields.DateTime,
    'json':    fields
}

# Instantiate the app
app = Flask(__name__)
api = Api(app)

# Database Setup
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb


parser = reqparse.RequestParser()
parser.add_argument('top', type=int, help="How many elements off the top you want")


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


class Laptop(Resource):
    def get(self):
        if db.tododb.count() == 0:
            return "No items were found"
        else:
            _items = db.tododb.find()
            items = [item for item in _items]
            return JSONEncoder().encode(items)

    def post(self):
        # Requests them from the form
        distance = request.form['name']
        brevet = request.form['description']
        date = request.form['date']
        time = request.form['time']

        # Input Validation
        if time is '' or brevet is '' or date is '' or distance is '':
            return redirect(url_for(Laptop)), 201

        # DateTime calculations
        datetime = date + " " + time
        start_time = arrow.get(datetime, "YYYY-M-D HH:mm")
        opentime = open_time(int(distance), int(brevet), start_time)
        closetime = close_time(int(distance), int(brevet), start_time)

        # Prep data for database
        item_doc = {
            'open_time': opentime,
            'close_time': closetime
        }

        # Insert into Mongodb
        db.tododb.insert_one(item_doc)
        return redirect('http://localhost:5000/')


class ListAll(Resource):
    def get(self):
        if db.tododb.count() == 0:
            return "No items were found"
        else:
            _items = db.tododb.find({}, {'_id': 0})
            items = [item for item in _items]
            return JSONEncoder().encode(items)


class ListOpen(Resource):
    def get(self):
        if db.tododb.count() == 0:
            return "No items were found"
        else:
            _items = db.tododb.find({}, {'open_time': 1, '_id': 0})
            items = [item for item in _items]
            return JSONEncoder().encode(items)


class ListClose(Resource):
    def get(self):
        if db.tododb.count() == 0:
            return "No items were found"
        else:
            _items = db.tododb.find({}, {'close_time': 1, '_id': 0})
            items = [item for item in _items]
            return JSONEncoder().encode(items)


class ListAllArg(Resource):
    def get(self, arg):

        # Implement top using by throwing in extra if conditionals:
        # args = parser.parse_args()
        # task = {'task': args['task']}
        flag = False
        argue = parser.parse_args()
        if argue is not None:
            flag = True
            number = argue['top']
        if db.tododb.count() == 0:
            return "No items were found"
        else:
            if flag:
                _items = db.tododb.find({}, {'_id': 0})[0:number]
            else:
                _items = db.tododb.find({}, {'_id': 0})
            items = [item for item in _items]
            if arg == 'csv':
                output = io.StringIO()
                csvdata = items
                writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
                writer.writerow(csvdata)
                return output.getvalue()
            else:
                return JSONEncoder().encode(items)


class ListOpenArg(Resource):
    def get(self, arg):
        flag = False
        argue = parser.parse_args()
        if argue is not None:
            flag = True
            number = argue['top']
        if db.tododb.count() == 0:
            return "No items were found"
        else:
            if flag:
                _items = db.tododb.find({}, {'open_time': 1, '_id': 0})[0:number]
            else:
                _items = db.tododb.find({}, {'open_time': 1, '_id': 0})
            items = [item for item in _items]
            if arg == 'csv':
                output = io.StringIO()
                csvdata = items
                writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
                writer.writerow(csvdata)
                return output.getvalue()
            else:
                return JSONEncoder().encode(items)


class ListCloseArg(Resource):
    def get(self, arg):
        flag = False
        argue = parser.parse_args()
        if argue is not None:
            flag = True
            number = argue['top']
        if db.tododb.count() == 0:
            return "No items were found"
        else:
            if flag:
                _items = db.tododb.find({}, {'close_time': 1, '_id': 0})[0:number]
            else:
                _items = db.tododb.find({}, {'close_time': 1, '_id': 0})
            items = [item for item in _items]
            if arg == 'csv':
                output = io.StringIO()
                csvdata = items
                writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
                writer.writerow(csvdata)
                return output.getvalue()
            else:
                return JSONEncoder().encode(items)


# Create routes
# Another way, without decorators
# Normal lists
api.add_resource(Laptop, '/')
api.add_resource(ListAll, '/listAll')
api.add_resource(ListOpen, '/listOpenOnly')
api.add_resource(ListClose, '/listCloseOnly')

# Now to add them if they have csv or json
api.add_resource(ListAllArg, '/listAll/<arg>')
api.add_resource(ListOpenArg, '/listOpenOnly/<arg>')
api.add_resource(ListCloseArg, '/listCloseOnly/<arg>')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
